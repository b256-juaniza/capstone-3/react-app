import { Navigate } from 'react-router-dom';
import { useEffect } from 'react';
import UserContext from '../userContext';
import { useContext } from 'react';


export default function Home() {

	const {user, setUser} = useContext(UserContext);

	useEffect(() => {
		if (user.id !== null && user.id !== undefined) {
        document.getElementById("topNav").style.display = "none";
      } else {
        document.getElementById("sideNav").style.display = "none";
        <Navigate to="/Register" />
      }
	})

	return (
		<Navigate to="/Dashboard" />
	)
}