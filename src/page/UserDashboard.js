import DisplayProduct from '../components/DisplayProduct';
import '../components/comp.css';
import { useEffect, useContext } from 'react';
import { Container } from 'react-bootstrap';
import { useState } from 'react';
import {Nav, Navbar} from 'react-bootstrap';
import {Link, NavLink, Navigate} from 'react-router-dom'; 
import UserContext from '../userContext';


export default function UserDashboard() {

	const [products, setProducts] = useState([]);
	const {user, setUser} = useContext(UserContext);
	
	useEffect(() => {	

		document.getElementById("sideNav").style.display = "flex";
		document.getElementById("sideNav").style.marginTop = "92vh";
		document.getElementById('sideNav').style.height = "100vh"; 

		fetch(`${process.env.REACT_APP_API_URL}/products/allproducts`)
		.then(res => res.json())
		.then(data => {
			setProducts(data.map(prods => {
				return(
					<>
						<DisplayProduct key={prods._Id} prods={ prods } />
					</> 
				)
			}))
		})
	})

	return (
		(user.id !== undefined && user.id !== null) ?
		<>
			<Container style={{height: "100%"}}>
				<div id="user-dash" className="row" /*style={{overscrollBehaviorInline: "contain", overscrollBehaviorX: "contain", overflowX: "scroll"}}*/ className="d-flex flex-row mt-4 p-3">
					<div className="row" style={{height: "100vh", marginLeft: "50px"}}>
						{products}
					</div>
				</div>
			</Container>
		</>
		:
		<Navigate to="/Login" />
	)
}